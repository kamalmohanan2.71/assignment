## Notes

1. `one.py` solves the first task. Simply run `./one.py` to see its output.

2. `two.patch` can be applied over the `polls` app given at `github.com/divio/django-polls`.

   `two.patch` implements an `Employee` model and the views to list and create `Employee` instances.

3. After setting up and running a project that uses the patched `polls` app, running `three.py` would fetch the data from the google sheet and POST it to the locally running server.


## Running the code

Clone this repo, create a venv and install all the dependencies.

```
$ cd /tmp
$ git clone git@gitlab.com:kamalmohanan2.71/assignment.git
$ cd assignment
$ python3.6 -m venv env
$ . env/bin/activate
$ pip install -r requirements.txt
```

At this point, you can run `one.py`.

Now, clone the `polls` app repo into the current directory, apply `two.patch` and install the app.

```
$ git clone git@github.com:divio/django-polls.git
$ cd django-polls/
$ git apply ../two.patch
$ cd ..
$ pip install -e django-polls/
```

Now create a sample django project in the current directory.

```
$ django-admin startproject sample
$ cd sample/
```

Add `'polls'` to `sample`'s `INSTALLED_APPS`.
Also add the following urlpattern in `urls.py`: `'path('polls/', include('polls.urls')),`

Now, migrate and run `sample`.

```
$ python manage.py migrate
$ python manage.py runserver
```

Once you get `sample` running, open another terminal, cd to `/tmp/assignment` and activate the venv.
Copy your `credentials.json` here and run `./three.py`.

Assuming everything worked so far,

```
$ curl localhost:8000/polls/employees/
```
should give you the list of employees.
