#!/usr/bin/env python3

import os.path
import pickle

import requests
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build


def fetch_sheet_values(sheetid, range):
    creds = None

    if os.path.exists('token.pickle'):
        with open('token.pickle', 'rb') as token:
            creds = pickle.load(token)

    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                'credentials.json',
                ['https://www.googleapis.com/auth/spreadsheets.readonly'])
            creds = flow.run_local_server(port=0)
        with open('token.pickle', 'wb') as token:
            pickle.dump(creds, token)

    service = build('sheets', 'v4', credentials=creds)

    sheet = service.spreadsheets()
    result = sheet.values().get(spreadsheetId=sheetid,
                                range=range).execute()

    return result.get('values', [])


if __name__ == '__main__':
    emps = fetch_sheet_values('13yyd8s008LlRn0tn6LC5moH1fcBELBkYw2THX6gjdHU',
                              'A2:D4')
    keys = ['first_name', 'last_name', 'employee_id', 'city']
    data_list = [dict(zip(keys, emp)) for emp in emps]

    for data in data_list:
        r = requests.post('http://localhost:8000/polls/employees/', data=data)
        if r.status_code != requests.codes.ok:
            r.raise_for_status()
